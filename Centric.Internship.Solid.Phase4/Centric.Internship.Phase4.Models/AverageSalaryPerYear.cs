﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase4.Models
{
    [Serializable()]
    public class AverageSalaryPerYear
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }

        [XmlElement(ElementName = "AverageSalary")]
        public double AverageSalary { get; set; }
    }
}