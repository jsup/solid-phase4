﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase4.Models
{
    [Serializable()]   
    public class Employee
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlArray(ElementName = "salaries")]
        public Salary[] Salaries { get; set; }
    }
}   