﻿namespace Centric.Internship.Phase4.App
{
    using Centric.Internship.Phase4.Business;
    using Centric.Internship.Phase4.Interfaces;

    public class SalaryApp
    {
        public void Run(IReaderService readerServices)
        {
            var salaryCalculator = new SalaryCalculator(readerServices);

            salaryCalculator.CalculateAverageSalary();
        }
    }
}
