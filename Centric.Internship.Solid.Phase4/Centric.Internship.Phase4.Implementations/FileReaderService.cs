﻿using Centric.Internship.Phase4.Interfaces;
using Centric.Internship.Phase4.Models;
using System.Collections.Generic;
using System.IO;

namespace Centric.Internship.Phase4.Implementations
{
    public class FileReaderService : IReaderService
    {
        private string _location;
        private IList<IContentParser> _contentParsers;

        public FileReaderService()
        {
            _contentParsers = new List<IContentParser>();
        }

        public void RegisterContentParser(IContentParser parser)
        {
            _contentParsers.Add(parser);
        }

        public Employees Read()
        {
            Employees employees = null;

            string input = File.ReadAllText(_location);

            foreach (var fileReader in _contentParsers)
            {
                if (fileReader.CanProcess(input))
                {
                    employees = fileReader.Process(input);
                    break;
                }
            }

            return employees;
        }

        public void SetLocation(string location)
        {
            _location = location;
        }
    }
}
