﻿namespace Centric.Internship.Phase4.Implementations
{
    using System.Collections.Generic;
    using System.Net.Http;

    using Centric.Internship.Phase4.Interfaces;
    using Centric.Internship.Phase4.Models;

    public class UrlReaderService : IReaderService
    {
        private string _location;

        private IList<IContentParser> _contentParsers;

        public UrlReaderService()
        {
            _contentParsers = new List<IContentParser>();
        }

        public Employees Read()
        {
            var httpClient = new HttpClient();
            
            var request = httpClient.GetAsync(this._location).Result;

            var response = request.Content.ReadAsStringAsync().Result;

            foreach (var parser in this._contentParsers)
            {
                if (parser.CanProcess(response))
                {
                    return parser.Process(response);
                }
            }

            return null;
        }

        public void RegisterContentParser(IContentParser parser)
        {
            _contentParsers.Add(parser);
        }

        public void SetLocation(string location)
        {
            this._location = location;
        }
    }
}
