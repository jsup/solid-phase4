﻿namespace Centric.Internship.Phase4.Tests
{
    using Centric.Internship.Phase4.App;
    using Centric.Internship.Phase4.Implementations;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalculateSalaryPerYear
    {
        [TestMethod]
        public void CalculateSalaryFromUrl()
        {
            var urlReaderService = new UrlReaderService();

            urlReaderService.RegisterContentParser(new JSONContentParser());

            // @"..\..\..\..\SalariesPerYear.json"
            urlReaderService.SetLocation("https://drive.google.com/uc?authuser=0&id=1KyWEmppDjkQv5SxbKZr4NS06DmmAq0j8&export=download");

            var app = new SalaryApp();

            app.Run(urlReaderService);
        }
        
        [TestMethod]
        public void CalculateSalaryFromFile()
        {
            var fileReaderService = new FileReaderService();

            fileReaderService.RegisterContentParser(new JSONContentParser());

            fileReaderService.SetLocation(@"..\..\..\..\SalariesPerYear.json");

            var app = new SalaryApp();

            app.Run(fileReaderService);
        }
    }
}