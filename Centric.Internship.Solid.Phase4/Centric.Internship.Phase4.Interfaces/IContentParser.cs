﻿namespace Centric.Internship.Phase4.Interfaces
{
    using Centric.Internship.Phase4.Models;

    public interface IContentParser
    {
        bool CanProcess(string content);
        Employees Process(string content);
    }
}
