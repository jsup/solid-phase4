﻿namespace Centric.Internship.Phase4.Interfaces
{
    using Centric.Internship.Phase4.Models;

    public interface IReaderService
    {
        void SetLocation(string location);

        void RegisterContentParser(IContentParser parser);
       
        Employees Read();
    }
}
